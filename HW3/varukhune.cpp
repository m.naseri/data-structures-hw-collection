#include <iostream>
#include <climits>
#include <string>
#include <algorithm>

using namespace std;
int** DP;
int** P;
string str;

int getMin(int i, int j) {
    if(DP[i][j] != -1)
        return DP[i][j];

    if(i > j)
        return DP[i][j] = 0;
    if(i == j) {
        P[i][j] = 0;
        return DP[i][j] = 0;
    }
//    if(i == j - 1) {
//        if(str[i] == str[j]) {
//
//            return DP[i][j] = 0;
//        }
//        else
//            return DP[i][j] = 1;
//    }

    if(str[i] == str[j]) {
        P[i][j] = 1;
        return DP[i][j] = getMin(i + 1, j - 1);
    }

    int M2 = getMin(i, j - 1);
    int M3 = getMin(i+1, j);
    if(M2 < M3) {
        P[i][j] = 2;
        return DP[i][j] = M2 + 1;
    } else if(M3 < M2) {
        P[i][j] = 3;
        return DP[i][j] = M3 + 1;
    } else {
        if(str[j] < str[i]) {
            P[i][j] = 2;
            return DP[i][j] = M2 + 1;
        } else {
            P[i][j] = 3;
            return DP[i][j] = M3 + 1;
        }
    }
}

// Driver program to test above functions 
int main() {
    cin >> str;
    DP = new int*[str.length()];
    P = new int*[str.length()];
    for(int i = 0; i < str.length(); i++) {
        DP[i] = new int[str.length()];
        P[i] = new int[str.length()];
        for(int j = 0; j < str.length(); j++) {
            DP[i][j] = -1;
            P[i][j] = -1;
        }
    }

    int minP = getMin(0, static_cast<int>(str.length() - 1));
//    cout << minP << endl;
    string ans;
    for(int i = 0; i < minP + str.length(); i++) ans += '0';

//    for(int i = 0; i < str.length(); i++) {
//        for(int j = 0; j < str.length(); j++) {
//            cout << P[i][j] << "\t";
//        }
//        cout << endl;
//    }

    int leftP = 0, rightP = static_cast<int>(ans.length() - 1),
            I = 0, J = static_cast<int>(str.length() - 1);
    while(leftP <= rightP) {
        if(P[I][J] == 1) {
            ans[leftP++] = str[I++];
            ans[rightP--] = str[J--];
        } else if(P[I][J] == 2) {
            ans[rightP--] = ans[leftP++] = str[J--];
        } else if(P[I][J] == 3) {
            ans[leftP++] = ans[rightP--] = str[I++];
        } else if(I == J) {
            ans[rightP--] = ans[leftP++] = str[I++];
        }
//        cout << ans << endl;
    }

    cout << ans << endl;
    return 0;
} 


