#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
typedef long long LL;
LL*** dp;
int tSum;
int * slice;
int sliceMax;
int n;

LL getCount(int diff, int level, int sMaxIndex) {
    if(diff < 0 || diff > tSum)
        return 0;
    if(level == 0) {
        if(diff == tSum/2)
            return 1;
        else
            return 0;
    }

    if(dp[diff][level][sMaxIndex] != -1)
        return dp[diff][level][sMaxIndex];

    int tempDiff = diff - slice[level-1];
    LL C1 = getCount(tempDiff, level - 1, max(sMaxIndex, level-1));

    LL C2 = getCount(diff, level - 1, sMaxIndex);

    LL C3 = 0;
    tempDiff = diff + slice[level-1];
    if(slice[sMaxIndex] <= slice[level-1]) {
        C3 = getCount(tempDiff, level - 1, sMaxIndex);
    }

    return dp[diff][level][sMaxIndex] = C1 + C2 + C3;
}

int main() {

    cin >> n;
    slice = new int[n];
    tSum = 0;
    sliceMax = INT32_MIN;
    for(int i = 0; i < n; i++) {
        cin >> slice[i];
        tSum += slice[i];
        sliceMax = max(sliceMax, slice[i]);
    }

    dp = new LL**[tSum+2];
    for(int i = 0;  i < tSum+1; i++) {
        dp[i] = new LL*[n+1];
        for(int j = 0; j < n+1; j++) {
            dp[i][j] = new LL[n+1];
            for(int k = 0; k < n+1; k++) {
                dp[i][j][k] = -1;
            }
        }
    }
    sort(slice, slice + n);

    cout << getCount(tSum/2, n, 0) - 1 << endl;

    return 0;
}
