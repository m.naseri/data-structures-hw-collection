SCORE: 75/100


// A C++ program for Dijkstra's single source shortest path algorithm. 
// The program is for adjacency matrix representation of the graph 
#include <iostream>
#include <cstdio>
#include <climits>
typedef unsigned long long ULL;
using namespace std;
// Number of vertices in the graph 
int V;
int* parent;
ULL* dist;
ULL** nR;

// A utility function to find the vertex with minimum distance value, from 
// the set of vertices not yet included in shortest path tree 
int minDistance(bool* sptSet) {
    // Initialize min value
    ULL min = dist[0];
    int min_index = 0;

    for (int v = 0; v < V; v++) {
        if (!sptSet[v] && dist[v] <= min) {
            min = dist[v];
            min_index = v;
        }
    }
    return min_index;
}

// Function that implements Dijkstra's single source shortest path algorithm
// for a graph represented using adjacency matrix representation
void dijkstra(ULL** graph, int src)
{
    bool sptSet[V]; // sptSet[i] will be true if vertex i is included in shortest
    // path tree or shortest distance from src to i is finalized

    // Initialize all distances as INFINITE and stpSet[] as false
    for(int i = 0; i < V; i++) {
        dist[i] = ULLONG_MAX;
        sptSet[i] = false;
    }

    // Distance of source vertex from itself is always 0
    dist[src] = 0;

    // Find shortest path for all vertices
    for(int count = 0; count < V-1; count++) {
        // Pick the minimum distance vertex from the set of vertices not
        // yet processed. u is always equal to src in the first iteration.
        int u = minDistance(sptSet);

        // Mark the picked vertex as processed
        sptSet[u] = true;

        // Update dist value of the adjacent vertices of the picked vertex.
        for (int v = 0; v < V; v++) {

            // Update dist[v] only if is not in sptSet, there is an edge from
            // u to v, and total weight of path from src to  v through u is
            // smaller than current value of dist[v]
            if(!sptSet[v] && graph[u][v] && dist[u] != ULLONG_MAX && graph[u][v] != ULLONG_MAX) {
                if(dist[u]+graph[u][v] < dist[v]) {
                    dist[v] = dist[u] + graph[u][v];
                    parent[v] = u;
                    nR[src][v] = nR[src][u] + nR[u][v];
                }
                else if(dist[u]+graph[u][v] == dist[v]) {
                    if(nR[src][u] + nR[u][v] < nR[src][v]) {
                        nR[src][v] = nR[src][u] + nR[u][v];
                        parent[v] = u;
                    }
                }

            }
        }
    }

    // print the constructed distance array
//    printSolution(dist, V);
}

// driver program to test above function 
int main() {
    V = 1000;
    auto ** graph = new ULL*[V];
    nR = new ULL*[V];
    for(int i = 0; i < V; i++) {
        graph[i] = new ULL[V];
        nR[i] = new ULL[V];
        for(int j = 0; j < V; j++) {
            graph[i][j] = ULLONG_MAX;
            nR[i][j] = ULLONG_MAX;
        }
        graph[i][i] = nR[i][i] = 0;
    }

    int x, y, n;
    cin >> x >> y >> n;
    x--;
    y--;

    auto * m = new ULL[n];
    auto * w = new ULL[n];

    for(int i = 0; i < n; i++) {
        cin >> w[i] >> m[i];
        auto * path = new int[m[i]];
        for(int j = 0; j < m[i]; j++) {
            cin >> path[j];
            path[j]--;
        }
        for(int j = 0; j < m[i]; j++) {
            for(int k = j+1; k < m[i]; k++) {
                if(w[i] < graph[path[j]][path[k]]) {
                    graph[path[j]][path[k]] = w[i];
                    nR[path[j]][path[k]] = static_cast<ULL>(k - j);
                } else if(w[i] == graph[path[j]][path[k]]) {
                    nR[path[j]][path[k]] = min(static_cast<ULL>(k - j), nR[path[j]][path[k]]);
                }
            }
        }
        delete[] path;
    }

    dist = new ULL[V];
    parent = new int[V];
    for(int i = 0; i < V; i++)
        parent[i] = -1;

    dijkstra(graph, x);

    if(dist[y] < ULLONG_MAX) {
        cout << dist[y] << " " << nR[x][y] << endl;
    } else {
        cout << -1 << " " << -1 << endl;
    }

    return 0;
} 