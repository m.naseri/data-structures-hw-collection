#include <bits/stdc++.h>

using namespace std;

list<int>* adj;
int V;

pair<int, int> bfs(int u) {
    int dis[V];
    for(int i = 0; i < V; i++) {
        dis[i] = -1; // not visited
    }

    queue<int> q;
    q.push(u);
    dis[u] = 0;

    while(!q.empty()) {
        int t = q.front();
        q.pop();
        for(auto it = adj[t].begin(); it != adj[t].end(); it++) {
            int v = *it;
            if (dis[v] == -1) {
                q.push(v);
                dis[v] = dis[t] + 1;
            }
        }
    }

    int maxDis = 0;
    int nodeIdx;
    for(int i = 0; i < V; i++) {
        if(dis[i] > maxDis) {
            maxDis = dis[i];
            nodeIdx = i;
        }
    }
    return pair<int, int>(nodeIdx, maxDis);
}

int main() {
    cin >> V;
    adj = new list<int>[V];

    for(int i = 0; i < V-1; i++) {
        int v1, v2;
        cin >> v1 >> v2;
        adj[v1 - 1].push_back(v2 - 1);
        adj[v2 - 1].push_back(v1 - 1);
    }

    pair<int, int> t1, t2;
    t1 = bfs(0);
    t2 = bfs(t1.first);

    cout << t2.second << endl;
    return 0;
}