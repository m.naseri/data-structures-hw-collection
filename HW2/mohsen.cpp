#include <iostream>
#include <limits>
#include <vector>
#include <string>

using namespace std;

class point {
public:
    point(int i, int j) {
        I = i;
        J = j;
    }
    int I, J;
};

int n, m, k;
point t(0, 0);
const bool fire = true;
vector<bool**> table;
vector<point> path;
int minTime[100][100][201];

bool exist(int i, int j) {
    return (i >= 0 && i < n && j >= 0 && j < m);
}

void makeFire(int index) {
    table.push_back(new bool*[n]);
    for(int i = 0; i < n; i++) {
        table[index + 1][i] = new bool[m];
    }

    for(int i = 0; i < n; i++) {
        for(int j = 0; j < m; j++) {
            table[index + 1][i][j] = table[index][i][j];
        }
    }
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < m; j++) {
            if(table[index][i][j] == fire) {
                if(exist(i-1, j-1))
                    table[index + 1][i-1][j-1] = fire;
                if(exist(i-1, j))
                    table[index + 1][i-1][j] = fire;
                if(exist(i-1, j+1))
                    table[index + 1][i-1][j+1] = fire;
                if(exist(i, j-1))
                    table[index + 1][i][j-1] = fire;
                if(exist(i, j+1))
                    table[index + 1][i][j+1] = fire;
                if(exist(i+1, j-1))
                    table[index + 1][i+1][j-1] = fire;
                if(exist(i+1, j))
                    table[index + 1][i+1][j] = fire;
                if(exist(i+1, j+1))
                    table[index + 1][i+1][j+1] = fire;
            }
        }
    }
}

int giveMinute(point s, int time) {
    if(time > 200)
        return numeric_limits<int>::max();
    if(minTime[s.I][s.J][time] != -1) {
        return minTime[s.I][s.J][time];
    }

    if(table.size() - 1 < time/k) {
        makeFire(time/k - 1);
    }

    if(table[time/k][s.I][s.J] == fire) {
        return minTime[s.I][s.J][time] = numeric_limits<int>::max();
    }

    if(s.I == t.I && s.J == t.J) {
        return minTime[s.I][s.J][time] = 0;
    }

    int M[4];
    for(int &i : M) {
        i = numeric_limits<int>::max();
    }

    if(exist(s.I-1, s.J))
        M[0] = giveMinute(point(s.I-1, s.J), time+1);
    if(exist(s.I+1, s.J))
        M[1] = giveMinute(point(s.I+1, s.J), time+1);
    if(exist(s.I, s.J-1))
        M[2] = giveMinute(point(s.I, s.J-1), time+1);
    if(exist(s.I, s.J+1))
        M[3] = giveMinute(point(s.I, s.J+1), time+1);

    int minM = min(M[0], min(M[1], min(M[2], M[3])));
    if(minM == numeric_limits<int>::max()) {
        return minTime[s.I][s.J][time] = numeric_limits<int>::max();
    }

    return minTime[s.I][s.J][time] = 1 + minM;
}

int main() {
    cin >> n >> m >> k;
    point s(0, 0);
    table.push_back(new bool*[n]);
    for(int i = 0; i < n; i++) {
        table[0][i] = new bool[m];
    }

    for(int l = 0; l < 100; l++) {
        for(int i = 0; i < 100; i++) {
            for(int j = 0; j < 201; j++) {
                minTime[l][i][j] = -1;
            }
        }
    }


    for(int i = 0; i < n; i++) {
        string tempStr;
        cin >> tempStr;
        for(int j = 0; j < m; ++j) {
            if(tempStr[j] == 'f') {
                table[0][i][j] = fire;
            }
            else if (tempStr[j]) {
                table[0][i][j] = !fire;
                if (tempStr[j] == 't') {
                    t.I = i;
                    t.J = j;
                }
                else if (tempStr[j] == 's') {
                    s.I = i;
                    s.J = j;
                }
            }
        }
    }

    int minute = giveMinute(s, 0);
    if(minute == numeric_limits<int>::max()) {
        cout << "Impossible" << endl;
    }
    else {
        cout << minute << endl;
    }

    return 0;
}