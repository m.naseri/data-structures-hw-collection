#include <iostream>
#include <queue>
#include <vector>

using namespace std;
typedef long long int LLI;

int main() {
    int n;
    cin >> n;
    priority_queue<LLI, vector<LLI>, greater<LLI>> pq;
    for(int i = 0; i < n; i++) {
        LLI temp;
        cin >> temp;
        pq.push(temp);
    }

    LLI counter = 0;
    while(true) {
        LLI a = pq.top();
        pq.pop();
        LLI b = pq.top();
        pq.pop();
        counter += a+b;
        if(pq.empty()) {
            counter += a+b;
            break;
        }
        pq.push(a+b);
    }
    cout << counter << endl;
    return 0;
}