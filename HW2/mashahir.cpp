#include <iostream>

using namespace std;

class myNode {
public:
    myNode* left;
    myNode* right;
    int key;
    int leftCounter;
};
myNode* root;

int kthSmallestNode(int k) {
    if(!root)
        return 0;

    myNode *pTraverse = root;
    int nodeKey = 0;
    while(pTraverse) {
        if((pTraverse->leftCounter + 1) == k) {
            nodeKey = pTraverse->key;
            break;
        }
        else if(k > pTraverse->leftCounter) {
            k = k - (pTraverse->leftCounter + 1);
            pTraverse = pTraverse->right;
        }
        else {
            pTraverse = pTraverse->left;
        }
    }
    return nodeKey;
}

void addNode(int inputKey) {
    myNode *node = new myNode;
    node->key = inputKey;
    node->leftCounter = 0;
    node->left = nullptr;
    node->right = nullptr;

    myNode *pTraverse = root;
    myNode *currentParent = root;

    while(pTraverse) {
        currentParent = pTraverse;

        if(node->key < pTraverse->key) {
            pTraverse->leftCounter++;
            pTraverse = pTraverse->left;
        }
        else {
            pTraverse = pTraverse->right;
        }
    }

    if(!root) {
        root = node;
    }
    else if(node->key < currentParent->key) {
        currentParent->left = node;
    }
    else {
        currentParent->right = node;
    }
}

int main() {
    int n;
    int arrSize = 0;
    cin >> n;
    root = nullptr;

    for(int i = 0; i < n; i++) {
        int qN;
        cin >> qN;
        if(qN == 1) {
            int tempN;
            cin >> tempN;
            addNode(tempN);
            arrSize++;
        } else {
            if(arrSize < 3) {
                cout << "No reviews yet" << endl;
            } else
                cout << kthSmallestNode(arrSize - arrSize / 3 + 1) << endl;
        }
    }
    return 0;
}