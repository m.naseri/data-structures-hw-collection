#include <iostream>
#include <string>
#include <stack>
#include <limits>
#include <list>
#include <climits>
//#include <bits/stdc++.h>

using namespace std;

int main() {
    int n;
    cin >> n;

    string str;
    cin >> str;

    stack<int> s;
    int max = INT_MIN;

    for(int i = 0; i < str.size(); i++) {
        if(str[i] == '(') {
            s.push(i);
        } else if(str[i] == ')') {
            int diff = i - s.top();
            if(diff > max) {
                max = diff;
            }
            s.pop();
        }
    }

    std::cout << max;


    return 0;
}