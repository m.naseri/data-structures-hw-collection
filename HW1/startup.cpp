#include <iostream>
#include <string>
#include <stack>
#include <limits>
#include <list>
//#include <bits/stdc++.h>


using namespace std;

class MyQueue {
public:
    MyQueue() {
    }

    void pushBack(int value) {
        l.push_back(value);
        lastCommand = "popBack";
    }
    void popFront() {
        lastCommand = "pushFront " + to_string(l.front());
        std::cout << l.front() << std::endl;
        l.pop_front();
    }

    void pushFront(int value) {
        l.push_front(value);
    }

    void popBack() {
        l.pop_back();
    }

    void undo() {
        if(lastCommand == "popBack") {
            popBack();
        } else {
            string pushFrontSTR = lastCommand.substr(0, std::min(lastCommand.size(), 9UL));
            if(pushFrontSTR == "pushFront") {
                int value = std::stoi(lastCommand.substr(10));
                pushFront(value);
            }
        }
    }

    list<int> l;
    string lastCommand;

};

int main() {
    MyQueue myQueue;
    int n;
    std::cin >> n;
    std::cin >> std::ws; // stream out any whitespace
    std::string* commands = new std::string[n];
    for(int i = 0; i < n; i++) {
        std::getline(std::cin, commands[i]);
//        std::cout << commands[i] << "\n";
    }

    for(int i = 0; i < n; i++) {
        std::string enqueueSTR = commands[i].substr(0, std::min(commands[i].size(), 7UL));
        std::string popSTR = commands[i].substr(0, std::min(commands[i].size(), 3UL));
        std::string undoSTR = commands[i].substr(0, std::min(commands[i].size(), 4UL));
        if(enqueueSTR == "enqueue") {
            int num = std::stoi(commands[i].substr(8));
            myQueue.pushBack(num);
        } else if (popSTR == "pop") {
            myQueue.popFront();
        } else if (undoSTR == "undo") {
            myQueue.undo();
        }
    }

    delete[] commands;


    return 0;
}