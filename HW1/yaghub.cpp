#include <iostream>
#include <string>
#include <stack>
#include <limits>
#include <list>
#include <climits>
#include <set>
//#include <bits/stdc++.h>

using namespace std;

int main() {
    int n, x;
    cin >> n >> x;
    multiset<int> A;
    for(int i = 0; i < n; i++) {
        int tempValue;
        cin >> tempValue;
        A.insert(tempValue);
    }

    int counter = 0;
    while(!A.empty()) {
        if(A.size() == 1) {
            counter++;
            break;
        }
        
        if(*(--A.end()) + *A.begin() <= x) {
            A.erase(--A.end());
            A.erase(A.begin());
            counter++;
        } else {
            A.erase(--A.end());
            counter++;
        }
    }
    std::cout << counter << endl;

    return 0;
}