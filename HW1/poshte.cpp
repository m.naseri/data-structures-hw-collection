//#include <iostream>
//#include <string>
//#include <stack>
//#include <limits>
#include <bits/stdc++.h>

class Node {
public:
    Node* previous;
    int value;
};

class MyStack {
public:
    MyStack() {
        top = nullptr;
        minStack.push(std::numeric_limits<int>::max());

    }

    ~MyStack() {
        while(top != nullptr) {
            Node* tempNode = top;
            top = top->previous;
            delete tempNode;
        }
    }

    void push(int value) {
        Node* newTop = new Node;
        newTop->previous = top;
        top = newTop;
        top->value = value;
        if(value <= minStack.top()) {
            minStack.push(value);
        }
    }

    int pop() {
        if(top == nullptr) {
            return -1;
        }
        Node* tempNode = top;
        top = top->previous;
        int tempVal = tempNode->value;
        delete tempNode;
        if(minStack.top() == tempVal) {
            minStack.pop();
        }

        return tempVal;
    }

    int findMin() {
        if(minStack.size() >= 2) {
            return minStack.top();
        }
        return -1;
    }

    std::stack<int> minStack;
    Node* top;
};

int main() {
    MyStack myStack;
    int n;
    std::cin >> n;
    std::cin >> std::ws; // stream out any whitespace
    std::string* commands = new std::string[n];
    for(int i = 0; i < n; i++) {
        std::getline(std::cin, commands[i]);
//        std::cout << commands[i] << "\n";
    }

    for(int i = 0; i < n; i++) {
        std::string pushSTR = commands[i].substr(0, std::min(commands[i].size(), 4UL));
        std::string popSTR = commands[i].substr(0, std::min(commands[i].size(), 3UL));
        std::string spellSTR = commands[i].substr(0, std::min(commands[i].size(), 5UL));
        if(pushSTR == "push") {
            int num = std::stoi(commands[i].substr(5));
            myStack.push(num);
        } else if (popSTR == "pop") {
            myStack.pop();
        } else if (spellSTR == "spell") {
            std::cout << myStack.findMin() << '\n';
        }
    }

    delete[] commands;
    return 0;
}